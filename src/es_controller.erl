-module(es_controller).

-behaviour(gen_server).

%% API
-export([start_link/1, stop/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([add/2, remove/1, list/0, save/0, load/0]).
-export([setf/3]).

-define(AUTOSAVE_INTERVAL, 60 * 1000).

-record(state, {tab, dir}).

%%%===================================================================
%%%
%%%===================================================================

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link(Dir) ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [Dir], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
stop() ->
  gen_server:cast(?MODULE, stop).

add(EsName, Type) ->
  gen_server:call(?MODULE, {add, EsName, Type}).

remove(EsName) ->
  gen_server:call(?MODULE, {remove, EsName}).

setf(EsName, FName, F) ->
  gen_server:call(?MODULE, {setf, EsName, FName, F}).

list() ->
  gen_server:call(?MODULE, list).

save() ->
  gen_server:call(?MODULE, save, infinity).

load() ->
  gen_server:call(?MODULE, load, infinity).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([Dir]) ->
  EsaTab = table_open(?MODULE, Dir),
  [es_tab:start_link(EsName, Dir, [{type, Type}, {mode, open}], Funs) || {EsName, {Type, Funs}} <- get_es_tables(EsaTab)],

  adz_logger:log("esc started"),

  {ok, #state{tab = EsaTab, dir = Dir}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_call({add, EsName, Type}, _From, State) ->
  Reply = case dets:member(State#state.tab, EsName) of
            true ->
              {ignore, has_table};
            false ->
              es_tab:start_link(EsName, State#state.dir, [{type, Type}, {mode, new}]),
              dets:insert(State#state.tab, [{EsName, {Type, []}}])
          end,

  {reply, Reply, State};

handle_call({remove, EsName}, _From, State) ->
  Reply = case dets:member(State#state.tab, EsName) of
    true ->
      es_tab:erase(EsName),
      dets:delete(State#state.tab, EsName);
    false -> {error, no_found}
  end,
  {reply, Reply, State};

handle_call(list, _From, State) ->
  Reply = get_es_names(State#state.tab),
  {reply, Reply, State};

handle_call(save, _From, State) ->
  EsList = get_es_names(State#state.tab),
  Reply = [{EsName, es_tab:save(EsName)} || EsName <- EsList],
  adz_logger:log("esc saved"),

  {reply, Reply, State};

handle_call(load, _From, State) ->
  EsList = get_es_names(State#state.tab),
  Reply = [{EsName, es_tab:load(EsName)} || EsName <- EsList],
  {reply, Reply, State};

handle_call({setf, EsName, FName, F}, _From, State) ->
  Reply = case dets:lookup(State#state.tab, EsName) of
            [{EsName, {Type, Funs}}] ->
              NewFuns = [{FName, F}|proplists:delete(FName, Funs)],
              dets:insert(State#state.tab, {EsName, {Type, NewFuns}}),
              es_tab:setf(EsName, FName, F);
            [] -> {error, no_found}
          end,

  {reply, Reply, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_cast(stop, State) ->
  [es_tab:stop(EsName) || EsName <- get_es_names(State#state.tab)],
  {stop, normal, State};

handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

table_open(Name, Dir) ->
  Filepath = lists:append(Dir, atom_to_list(Name)),
  case dets:open_file(Filepath) of
    {ok, Tab} ->
      Tab;
    {error, _} ->
      Options = [
        {file, Filepath},
        {repair, true},
        {auto_save, ?AUTOSAVE_INTERVAL}
      ],
      {ok, Tab} = dets:open_file(Name, Options),
      Tab
  end.

get_es_names(EsaTab) ->
  dets:foldl(fun({Name, _}, Acc) -> [Name|Acc] end, [], EsaTab).

get_es_tables(EsaTab) ->
  dets:foldl(fun(I, Acc) -> [I|Acc] end, [], EsaTab).
