-module(es_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

-define(CRON_TASKS, [
    {es_controller_save, {es_controller, save, []}, 3600 * 1000, false}
]).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
  RootDir = get_app_dir(),

  {ok, { {one_for_one, 5, 10}, [
    {ecode_sup, {ecode_sup, start_link, [RootDir]}, permanent, brutal_kill, supervisor, [ecode_sup]},
    {adz_logger, {adz_logger, start_link, [RootDir ++ "logs/log.txt"]}, permanent, brutal_kill, worker, [adz_logger]},

    {es_controller, {es_controller, start_link, [RootDir ++ "saves/"]}, permanent, brutal_kill, worker, [es_controller]},

    {adz_cron, {adz_cron, start_link, [?CRON_TASKS]}, permanent, brutal_kill, worker, [adz_cron]}
  ]} }.

get_app_dir() ->
  {ok, Path} = file:get_cwd(),
  Path ++ "/".
