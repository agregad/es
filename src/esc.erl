-module(esc).

-behaviour(gen_server).

%% API
-export([start_link/2]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([add/3, add/4, remove/1, list/0, list/1, spec/1, setf/3, save/0, load/0]).

-define(SERVER, esc).
-define(AUTOSAVE_INTERVAL, 60 * 1000).

-record(state, {tab, eslist, nodes=[]}).
-compile(export_all).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link(Dir, Nodes) ->
  gen_server:start_link({global, ?SERVER}, ?MODULE, [Dir, Nodes], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
%% Node = 'node_atom'|auto
add(Name, Type, Node) ->
  add(Name, Type, Node, [none]).

add(Name, Type, Node, Labels) ->
  gen_server:call({global, ?SERVER}, {add, Name, Type, Node, Labels}).

remove(Name) ->
  gen_server:call({global, ?SERVER}, {remove, Name}).

list() ->
  gen_server:call({global, ?SERVER}, list).

list(Label) ->
  gen_server:call({global, ?SERVER}, {list, Label}).

spec(Name) ->
  gen_server:call({global, ?SERVER}, {spec, Name}).

setf(Name, FName, F) ->
  gen_server:call({global, ?SERVER}, {setf, Name, FName, F}).

save() ->
  gen_server:call({global, ?SERVER}, save).

load() ->
  gen_server:call({global, ?SERVER}, load).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([Dir, Nodes]) ->
  Tab = table_open(?SERVER, Dir),
  EsList = orddict:from_list(get_es_spec_list(Tab)),
  State = #state{
    tab = Tab,
    eslist = EsList,
    nodes = Nodes
  },

  {ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_call({add, Name, Type, Node, Labels}, _From, State) ->
  LowUsageNode = case Node == auto of
    true -> get_low_usage_node(State#state.nodes);
    false -> Node
  end,
  es_node:add(LowUsageNode, Name, Type),

  dets:insert(State#state.tab, {Name, {LowUsageNode, Labels}}),
  NewState = State#state{eslist = orddict:store(Name, LowUsageNode, State#state.eslist)},

  Reply = {Name, LowUsageNode},
  {reply, Reply, NewState};

handle_call({remove, Name}, _From, State) ->
  {Reply, NewState} = case dets:lookup(State#state.tab, Name) of
    [] ->
      {
        {error, not_found},
        State
      };
    [{Name, {Node, _}}] ->
      dets:delete(State#state.tab, Name),
      {
        {ok, es_node:remove(Node, Name)},
        State#state{eslist = orddict:erase(Name, State#state.eslist)}
      }
  end,
  {reply, Reply, NewState};

handle_call(list, _From, State) ->
  Reply = get_es_spec_list(State#state.tab),
  {reply, Reply, State};

handle_call({list, Label}, _From, State) ->
  Reply = dets:foldl(
    fun({Name, {Node, Labels}}, Acc) ->
      case lists:member(Label, Labels) of
        true -> [{Name, Node}|Acc];
        false -> Acc
      end
    end,
    [],
    State#state.tab
  ),
  {reply, Reply, State};

handle_call({spec, Name}, _From, State) ->
  Reply = case orddict:find(Name, State#state.eslist) of
            {ok, Node} -> {Name, Node};
            error -> null
          end,
  {reply, Reply, State};

handle_call({setf, Name, FName, F}, _From, State) ->
  Reply = case dets:lookup(State#state.tab, Name) of
            [] ->
              {error, not_found};
            [{Name, {Node, _}}] ->
              {ok, es_node:setf(Node, Name, FName, F)}
          end,
  {reply, Reply, State};

handle_call(save, _From, State) ->
  Reply = [{Node, es_node:save(Node)} || Node <- State#state.nodes],
  {reply, Reply, State};

handle_call(load, _From, State) ->
  Reply = [{Node, es_node:load(Node)} || Node <- State#state.nodes],
  {reply, Reply, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
table_open(Name, Dir) ->
  Filepath = lists:append(Dir, atom_to_list(Name)),
  case dets:open_file(Filepath) of
    {ok, Tab} ->
      Tab;
    {error, _} ->
      Options = [
        {file, Filepath},
        {repair, true},
        {auto_save, ?AUTOSAVE_INTERVAL}
      ],
      {ok, Tab} = dets:open_file(Name, Options),
      Tab
  end.

get_es_spec_list(Tab) ->
  dets:foldl(fun({Name, {Node, _}}, Acc) -> [{Name, Node}|Acc] end, [], Tab).

get_low_usage_node(Nodes) ->
  NList = [{Node, get_ets_memory_usage(Node)} || Node <- Nodes],
  [{Node, _}|_] = lists:sort(fun({_, A}, {_, B}) -> A < B end, NList),
  Node.

get_ets_memory_usage(Node) ->
  rpc:call(Node, erlang, memory, [ets]).
