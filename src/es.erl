-module(es).

%% API
-export([cast/3, call/3, call/4]).

cast(EsSpec, Fun, Args) when is_function(Fun) ->
  es_tab_run(EsSpec, cast, [Fun, Args]);

cast(EsSpec, FName, Args) ->
  es_tab_run(EsSpec, cast, [FName, Args]).

call(EsSpec, Fun, Args) when is_function(Fun) ->
  es_tab_run(EsSpec, call, [Fun, Args]);

call(EsSpec, FName, Args) ->
  es_tab_run(EsSpec, call, [FName, Args]).

call(EsSpec, Fun, Args, Timeout) when is_function(Fun) ->
  es_tab_run(EsSpec, call, [Fun, Args, Timeout]);

call(EsSpec, FName, Args, Timeout) ->
  es_tab_run(EsSpec, call, [FName, Args, Timeout]).

%%%===================================================================
%%% Internal functions
%%%===================================================================
es_tab_run({Name, Node}, Fun, Args) ->
  erlang:apply(rpc, Fun, [Node, es_tab, Fun, [Name|Args]]).
