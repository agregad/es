-module(es_node).

%% API
-export([add/3, remove/2, list/1, setf/4, save/1, load/1]).

add(Node, EsName, Type) ->
  es_tab_c_run(Node, add, [EsName, Type]).

remove(Node, EsName) ->
  es_tab_c_run(Node, remove, [EsName]).

setf(Node, EsName, FName, F) ->
  es_tab_c_run(Node, setf, [EsName, FName, F]).

list(Node) ->
  es_tab_c_run(Node, list, []).

save(Node) ->
  es_tab_c_run(Node, save, []).

load(Node) ->
  es_tab_c_run(Node, load, []).

%%%===================================================================
%%% Internal functions
%%%===================================================================
es_tab_c_run(Node, Fun, Args) ->
  rpc:call(Node, es_controller, Fun, Args).
