-module(es_tab).

-behaviour(gen_server).

%% API
-export([start_link/3, start_link/4, stop/1, erase/1]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-export([f1/0, f2/0]).
-export([setf/3, cast/3, call/3, call/4]).
-export([save/1, load/1]).

-record(state, {name, tab, dir, funs}).

%%%===================================================================
%%% API
%%%===================================================================

f1() ->
  fun(Tab, Values) -> ets:insert(Tab, Values) end.

f2() ->
  fun(Tab, Key, Default) -> case ets:lookup(Tab, Key) of [{Key, Value}] -> Value; [] -> Default end end.

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link(EsName, Dir, Options) ->
  start_link(EsName, Dir, Options, []).

start_link(EsName, Dir, Options, Funs) ->
  gen_server:start_link({local, EsName}, ?MODULE, [EsName, Dir, Options, Funs], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================
stop(EsName) ->
  gen_server:cast(EsName, stop).

erase(EsName) ->
  gen_server:cast(EsName, erase).

save(EsName) ->
  gen_server:call(EsName, save, infinity).

load(EsName) ->
  gen_server:call(EsName, load, infinity).

setf(EsName, FName, F) ->
  gen_server:cast(EsName, {set, FName, F}).

cast(EsName, Fun, Args) when is_function(Fun) ->
  gen_server:cast(EsName, {apply, Fun, Args});

cast(EsName, FName, Args) ->
  gen_server:cast(EsName, {apply, FName, Args}).

call(EsName, Fun, Args) when is_function(Fun) ->
  gen_server:call(EsName, {apply, Fun, Args});

call(EsName, FName, Args) ->
  gen_server:call(EsName, {apply, FName, Args}).

call(EsName, Fun, Args, Timeout) when is_function(Fun) ->
  gen_server:call(EsName, {apply, Fun, Args}, Timeout);

call(EsName, FName, Args, Timeout) ->
  gen_server:call(EsName, {apply, FName, Args}, Timeout).

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([EsName, Dir, Options, Funs]) ->
  TabType = proplists:get_value(type, Options, set),
  EsTab = case proplists:get_value(mode, Options, new) of
    new -> table_create(Dir, EsName, TabType);
    open -> table_open(Dir, EsName, TabType)
  end,
  {ok, #state{name = EsName, tab = EsTab, dir = Dir, funs = Funs}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_call(save, _From, State) ->
  Reply = table_save(State),
  {reply, Reply, State};

handle_call(load, _From, State) ->
  EsTab = table_load(State),
  {reply, EsTab, State#state{tab = EsTab}};

handle_call({apply, Fun, Args}, _From, State) when is_function(Fun) ->
  Response = erlang:apply(Fun, [State#state.tab|Args]),
  {reply, Response, State};

handle_call({apply, FName, Args}, _From, State) ->
  Response = apply_fun({FName, Args}, State),
  {reply, Response, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).

handle_cast({set, Name, F}, State) ->
  Funs = proplists:delete(Name, State#state.funs),
  {noreply, State#state{funs = [{Name, F}|Funs]}};

handle_cast({apply, Fun, Args}, State) when is_function(Fun) ->
  erlang:apply(Fun, [State#state.tab|Args]),
  {noreply, State};

handle_cast({apply, FName, Args}, State) ->
  apply_fun({FName, Args}, State),
  {noreply, State};

handle_cast(stop, State) ->
  table_save(State),
  {stop, normal, State};

handle_cast(erase, State) ->
  file:delete(get_tab_file(State#state.dir, State#state.name)),
  {stop, normal, State};

handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

table_create(Dir, EsName, TabType) ->
  EsTab = ets:new(EsName, [TabType, named_table, public]),
  ets:tab2file(EsName, get_tab_file(Dir, EsName)),
  EsTab.

table_open(Dir, EsName, TabType) ->
  case ets:file2tab(get_tab_file(Dir, EsName)) of
    {ok, EsTab} -> EsTab;
    {error, _} -> table_create(Dir, EsName, TabType)
  end.

table_save(State) ->
  Filepath = get_tab_file(State#state.dir, State#state.name),
  ets:tab2file(State#state.tab, Filepath).

table_load(State) ->
  EsTab = State#state.tab,
  Filepath = get_tab_file(State#state.dir, State#state.name),
  ets:delete(EsTab),
  {ok, EsTab} = ets:file2tab(Filepath),
  EsTab.


apply_fun({Name, Args}, State) ->
  case proplists:get_value(Name, State#state.funs) of
    undefined -> {error, undefined_function};
    F -> erlang:apply(F, [State#state.tab|Args])
  end.

get_tab_file(Dir, EsName) ->
  lists:append(Dir, EsName).
