#!/bin/bash

erl -pa ebin/ ext/ deps/*/ebin/ \
    -name $1@$2 \
    -setcookie ABC
