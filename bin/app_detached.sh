#!/bin/bash

./bin/compile.sh

erl -pa ebin/ ext/ deps/*/ebin/ \
    -env ERL_MAX_ETS_TABLES 10000 \
    -name $1@$2 \
    -setcookie ABC \
    -config config/$3 \
    -boot start_sasl \
    -eval "application:start(es)." \
    -detached
